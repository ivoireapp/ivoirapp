<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pin extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function pinsNews(){
        return $this->hasMany('ivoirapp\News');
    }
}
