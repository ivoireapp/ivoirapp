<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    public function communeMaquis(){
        return $this->hasMany('ivoirapp\Maqui');
    }

}
