<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actualite extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    //protected $table = 'actualites';
}
