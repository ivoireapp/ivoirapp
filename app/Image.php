<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function ImageAlbums(){
        return $this->belongsTo('ivoirapp\Albums','albums_id');
    }
}
