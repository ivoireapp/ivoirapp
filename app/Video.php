<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function videoAlbums(){
        return $this->belongsTo('ivoirapp\Albums','albums_id');
    }
}
