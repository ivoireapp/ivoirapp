<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'actualites';

    protected $dates = ['deleted_at'];

    public function PinsActus(){
        return $this->belongsTo('ivoirapp\Pin','pin_id');
    }

    public function AlbumActus(){
        return $this->hasMany('ivoirapp\Albums','actualite_id');
    }
}
