<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;

class Albums extends Model
{
    protected $table = 'albums';

    public function albumActus(){
        return $this->belongsTo('ivoirapp\News','actualite_id');
    }

    public function albumImages(){
        return $this->hasMany('ivoirapp\Image','albums_id');
    }

    public function albumVideo(){
        return $this->hasMany('ivoirapp\Video','albums_id');
    }
}
