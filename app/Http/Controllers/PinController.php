<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use ivoirapp\Pin;
use Illuminate\Http\Request;

class PinController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPins = Pin::all();
        return view('admin/pins/index',compact('allPins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'lat' =>'required',
            'lon' =>'required',
            'name' =>'required',
        ]);
        if($valider->fails()){
            return redirect()->route('pins.index')->withErrors($valider->errors());
        }else{
            if($request->hasFile('file')){

                $pins = new Pin();
                $pins->slug= str_slug(str_random(8));
                $pins->name = $reponse['name'];
                $pins->des = $reponse['contenu'];
                $pins->lat = $reponse['lat'];
                $pins->lon = $reponse['lon'];

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName = 'piges';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $pins->img = $picture;
                $pins->save();

                return redirect()->route('pins.index')->with('success','✔ Félicitation ! vous venez d\' ajoute une pige');

            }else{
                return redirect()->route('pins.index')->with('error','Veuillez inserez la Photo du panneau');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \ivoirapp\Pin  $pin
     * @return \Illuminate\Http\Response
     */
    public function show(Pin $pin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ivoirapp\Pin  $pin
     * @return \Illuminate\Http\Response
     */
    public function edit(Pin $pin)
    {
        //dd($pin->id);
        $itempin = Pin::find($pin->id);
        //dd($itempin);
        return view('admin/pins/edit',compact('itempin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ivoirapp\Pin  $pin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pin $pin)
    {
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'lat' =>'required',
            'lon' =>'required',
            'name' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('pins.index')->withErrors($valider->errors());
        }else{

            $pins = Pin::find($pin->id);

            if($request->hasFile('file')){
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'piges';
                $picture = str_random(8).'.'. $extension;

                if (!empty($pins->img)) {
                    unlink($folderName.'/'.$pins->img);
                }
                $pins->img = $picture;
                $file->move($folderName,$picture);
            }

            $pins->name = $reponse['name'];
            $pins->des = $reponse['contenu'];
            $pins->lat = $reponse['lat'];
            $pins->lon = $reponse['lon'];
            $pins->save();

            return redirect()->route('pins.index')->with('success','✔ Félicitation ! vous venez de modifier un pin');

            //dd($reponse);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ivoirapp\Pin  $pin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pin $pin)
    {
        $pins = Pin::find($pin->id);
        $pins->delete();
        return redirect()->route('pins.index')->with('success','vous venez de supprimer un pin');
    }
}
