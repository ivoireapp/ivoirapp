<?php

namespace ivoirapp\Http\Controllers;

use ivoirapp\Actualite;
use Illuminate\Http\Request;
use ivoirapp\Pin;

class ActualiteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd("e");
        $allpins = Pin::all();
        return view('admin/actu/index',compact('allpins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'title' =>'required',
            //'contenu' =>'required',
            //'idpins' =>'required',
        ]);
        if($valider->fails()){
            return redirect()->route('actualites.index')->withErrors($valider->errors());
        }else{
            if($request->hasFile('file')){

                $actus = new Actualite();
                $actus->slug= str_slug(str_random(8));
                $actus->title = $reponse['title'];
                $actus->des = $reponse['contenu'];
                $actus->pin_id = $reponse['idpins'];

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName = 'actualites';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $actus->imgactu = $picture;
                $actus->save();

                return redirect()->route('actualites.index')->with('success','✔ Félicitation ! vous venez d\' ajoute une actualite');

            }else{
                return redirect()->route('actualites.index')->with('error','Veuillez inserez la Photo de l\'actualité');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \ivoirapp\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function show(Actualite $actualite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ivoirapp\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function edit(Actualite $actualite)
    {
        $itemactu = Actualite::find($actualite->id);
        $allpins = Pin::all();
        return view('admin/actu/edit',compact('itemactu','allpins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ivoirapp\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actualite $actualite)
    {
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'title' =>'required',
            //'contenu' =>'required',
            //'idpins' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('actualites.index')->withErrors($valider->errors());
        }else{

            $actus = Actualite::find($actualite->id);

            if($request->hasFile('file')){
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'actualites';
                $picture = str_random(8).'.'. $extension;

                if (!empty($actus->imgactu)) {
                    unlink($folderName.'/'.$actus->imgactu);
                }
                $actus->imgactu = $picture;
                $file->move($folderName,$picture);
            }

            $actus->title = $reponse['title'];
            $actus->des = $reponse['contenu'];
            $actus->pin_id = $reponse['idpins'];
            $actus->save();

            return redirect()->route('actualites.index')->with('success','✔ Félicitation ! vous venez de modifier une actualite');

            //dd($reponse);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ivoirapp\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actualite $actualite)
    {
        $pins = Actualite::find($actualite->id);
        $pins->delete();
        return redirect()->route('actualites.index')->with('success','vous venez de supprimer une actualite');
    }
}
