<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ivoirapp\Albums;
use ivoirapp\Image;
use ivoirapp\News;
use ivoirapp\Video;

class GallerieController extends Controller
{
    public function index(){
        $allactus = Albums::with('albumActus')->get();
        //dd($allactus);
        $allactu = News::all();

        /*$imgsearch = Image::where('albums_id',$allactus->id)->count();
        $vdeosearch = Video::where('albums_id',$allactus->id)->count();*/

        return view('admin/gallery/index',compact('allactus','allactu'));
    }

    public function store(Request $request){
        $requete = $request->except(['_token']);
        $albums = new Albums();

        $albums->title = $requete['title'];
        $albums->slug = str_slug(str_random(8));
        $albums->actualite_id = $requete['idactu'];
        $albums->save();

        if ($albums->id) {
            return redirect()->route('gallery');
        } else {
            return redirect()->route('gallery');
        }
    }

    public function show($slug){
        //dd($slug);
        $albums = Albums::where('slug',$slug)->first();
        $images = Image::where('albums_id',$albums->id)->with('ImageAlbums')->get();
        $videos = Video::where('albums_id',$albums->id)->with('videoAlbums')->get();
        //dd($images);
        return view('admin/gallery/show',compact('images','albums','videos'));
    }

    public function storeImg(Request $request){
        $requete = $request->except(['_token']);
        //dd($requete);
        if($request->hasFile('file')){
            $albums = Albums::find($requete['idalbum']);

            try{
                $actus = new Image();
                $actus->albums_id=$requete['idalbum'] ;

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName = 'galleries';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $actus->imgs = $picture;
                $actus->save();
            }catch (\Exception $e){
                dd($e->getMessage());
            }

            return redirect('gallery/'.$albums->slug);

        }else{
            return redirect()->route('gallery');
        }
    }

    public function storeVideo(Request $request){
        $requete = $request->except(['_token']);
        //dd($requete);
        $albums = Albums::find($requete['idalbum']);
        $takecodeVd = explode('watch?v=', $requete['urlyoutube']);
        $code = $takecodeVd[1];

        try{
            $video = new Video();
            $video->albums_id=$requete['idalbum'] ;
            $video->url=$code ;
            $video->save();
        }catch (\Exception $e){
            dd($e->getMessage());
        }
        return redirect('gallery/'.$albums->slug);

    }

    public function delImg($id){
        if($id){
            $image = Image::find($id);
            if (!empty($image->imgs)) {
                $folderName = 'galleries';
                unlink($folderName.'/'.$image->imgs);
                $image->delete();

                $albums = Albums::find($image->albums_id);
                return redirect('gallery/'.$albums->slug);
            }
        }
    }

    public function delVideo($id){
        if($id){
            $video = Video::find($id);
            $video->delete();
            $albums = Albums::find($video->albums_id);
            return redirect('gallery/'.$albums->slug);
        }
    }

    public function edit($id){
        //dd($id);
        if($id){
            $allactus = News::all();
            $itemAlbums = Albums::where('slug',$id)->first();
            //dd($itemAlbums);
            return view("admin/gallery/edit",compact('itemAlbums','allactus'));
        }
    }

    public function update($id , Request $request){
        $findAlbum = Albums::find($id);
        $requete = $request->except(['_token']);
        //dd($requete);
        $findAlbum->title = $requete['title'];
        $findAlbum->actualite_id = $requete['idactu'];
        $findAlbum->save();

        if ($findAlbum->id) {
            return redirect()->route('gallery');
        } else {
            return redirect()->route('gallery');
        }

    }

    public function del($id){
        //dd($id);
        if($id){
            $findAlbum = Albums::where('slug',$id)->first();
            $imgsearch = Image::where('albums_id',$findAlbum->id)->count();
            $vdeosearch = Video::where('albums_id',$findAlbum->id)->count();
            if($imgsearch==0 AND $vdeosearch==0){
                $findAlbum->delete();
                return redirect()->route('gallery');
            }else{
                return redirect()->route('gallery');
            }

            /*//supprimer tous les images
            $image = DB::table('images')->where('albums_id',$findAlbum->id)->delete();
            //supprimer tous les videos
            $video = Video::where('albums_id',$findAlbum->id)->get();
            $video->delete();*/
        }
    }
}
