<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use ivoirapp\Http\Controllers\Controller;

use ivoirapp\News;
use ivoirapp\Pin;
use JavaScript;

class FrontEndController extends Controller
{
    public function index()
    {
    	$actus = News::with('PinsActus')->whereNull('deleted_at')->orderBy('id', 'DESC')->limit(6)->get();
        $allPins = Pin::with('pinsNews')->get();
        $allPins->map(function($item){
            $tab= explode(' ', strip_tags($item->des), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });
        //dd($allPins);
        JavaScript::put(compact('allPins'));
    	return view('desktop/index', compact('actus'));
    }

    public function map()
    {
        $allPins = Pin::with('pinsNews')->get();
        $allPins->map(function($item){
            $tab= explode(' ', strip_tags($item->des), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });
        //dd($allPins);
        JavaScript::put(compact('allPins'));
    	return view('desktop/map');
    }

    public function actualites()
    {
    	$actus = News::with('PinsActus')->whereNull('deleted_at')->orderBy('id', 'DESC')->paginate(12);
    	return view('desktop/actualites', compact('actus'));
    }

    public function actualite($slug)
    {
    	$actu = News::where('slug', $slug)->get();
    	return view('desktop/actualite', compact('actu'));
    }
}
