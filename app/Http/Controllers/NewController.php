<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use ivoirapp\News;
use ivoirapp\Pin;

class NewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allpins = Pin::all();
        $allactus = News::with('PinsActus')->orderBy('datepublie', 'des')->get();
        //dd($allactus);
        return view('admin/news/index',compact('allpins','allactus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reponse = $request->except(['_token']);
        //dd($reponse);

        $valider = Validator::make($request->all(),[
            'title' =>'required',
            'contenu' =>'required',
            'idpins' =>'required',
        ]);
        if($valider->fails()){
            return redirect()->route('news.index')->withErrors($valider->errors());
        }else{
            $datepublie= $reponse['datepublie'];
            //$datenow = date('Y-m-d');
            $stat = "publier";

            /*if (strtotime($datepublie) == strtotime($datenow)){
                $stat = "publier";
            }elseif(strtotime($datepublie) > strtotime($datenow)){
                $stat = "en attente";
            }else{
                $stat = "mauvais";
            }*/

            if($stat=="mauvais"){
                return redirect()->route('news.index')->with('error','Veuillez inserez une date de publication exacte');
            }
            if($request->hasFile('file')){

                try{
                    $actus = new News();
                    $actus->slug= str_slug(str_random(8));
                    $actus->title = $reponse['title'];
                    $actus->des = $reponse['contenu'];
                    $actus->pin_id = $reponse['idpins'];

                    $file = $request->file('file');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    //$folderName = '../public_html/piges/panneau';
                    $folderName = 'actualites';
                    $picture = str_random(8).'.'. $extension;
                    $file->move($folderName,$picture);

                    $actus->imgactu = $picture;
                    $actus->datepublie = $datepublie;
                    $actus->status = $stat;
                    $actus->save();
                }catch (\Exception $e){
                    dd($e->getMessage());
                }


                return redirect()->route('news.index')->with('success','✔ Félicitation ! vous venez d\' ajoute une actualite');

            }else{
                return redirect()->route('news.index')->with('error','Veuillez inserez la Photo de l\'actualité');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemactu = News::find($id);
        //dd($itemactu);
        $allpins = Pin::all();
        return view('admin/news/edit',compact('itemactu','allpins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $reponse = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'title' =>'required',
            'contenu' =>'required',
            'idpins' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('news.index')->withErrors($valider->errors());
        }else{

            $actus = News::find($id);

            if($request->hasFile('file')){
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'actualites';
                $picture = str_random(8).'.'. $extension;

                if (!empty($actus->imgactu)) {
                    unlink($folderName.'/'.$actus->imgactu);
                }
                $actus->imgactu = $picture;
                $file->move($folderName,$picture);
            }

            $actus->title = $reponse['title'];
            $actus->des = $reponse['contenu'];
            $actus->pin_id = $reponse['idpins'];
            $actus->save();

            return redirect()->route('news.index')->with('success','✔ Félicitation ! vous venez de modifier une actualite');

            //dd($reponse);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pins = News::find($id);
        $pins->delete();
        return redirect()->route('news.index')->with('success','vous venez de supprimer une actualite');
    }

    public function newactuel(Request $request)
    {
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $idslug= $reponse['idactunew'];

        try{
            DB::table('uneactu')->insert(['slug' => $idslug]);
            return redirect()->route('news.index');
        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }

}
