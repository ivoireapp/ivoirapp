<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use ivoirapp\Commune;
use ivoirapp\Maqui;

class MaquiController extends Controller
{
    public function index(){
        $allmaquis = Maqui::orderBy('libelle','asc')->with('maquisCommune')->get();
        $allcommunes = Commune::orderBy('commune','asc')->get();
        //dd($allmaquis);
        return view('admin/maquis/index',compact('allmaquis','allcommunes'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        try{
            $maqui = new Maqui();
            $maqui->libelle = $reponse['libelle'];
            $maqui->des = $reponse['des'];
            $maqui->commune_id = $reponse['communid'];
            $maqui->save();
        }catch (\Exception $e){
            dd($e->getMessage());
        }
        return redirect()->route('maquis');



    }

    public function del($id){
        //dd($id);
        $maqui = Maqui::find($id);
        $maqui->delete();
        return redirect()->route('maquis');
    }
}
