<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use ivoirapp\Commune;

class CommuneController extends Controller
{
    public function index(){
        $allcommunes = Commune::orderBy('commune','asc')->get();
        //dd($allcommunes);
        return view('admin/commune/index',compact('allcommunes'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        try{
            $intro = new Commune();
            $intro->commune = $reponse['commune'];;
            $intro->save();
        }catch (\Exception $e){
            dd($e->getMessage());
        }
        return redirect()->route('commune');
    }

    public function del($id){
        $comm = Commune::with('communeMaquis')->find($id);
        $count = count($comm->communeMaquis);
        if ($count<=0){
            $comm->delete();
            return redirect()->route('commune');
        }else{
            return redirect()->route('commune');
        }

    }
}
