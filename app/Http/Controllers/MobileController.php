<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ivoirapp\Albums;
use ivoirapp\Commune;
use ivoirapp\Http\Controllers\Controller;
use ivoirapp\Image;
use ivoirapp\Maqui;
use ivoirapp\News;
use ivoirapp\Pin;
use ivoirapp\Slide;
use ivoirapp\Video;
use JavaScript;

class MobileController extends Controller
{
    public function index()
    {
    	return view('mobile/intro');
    }

    public function map()
    {
        $allPins = Pin::with('pinsNews')->get();
        $allPins->map(function($item){
            $tab= explode(' ', strip_tags($item->des), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });
       //dd($allPins);
        JavaScript::put(compact('allPins'));
    	return view('mobile/map');
        //return json_encode(array('data'=>$allPins));
    }

    public function workermap()
    {
        $allPins = Pin::with('pinsNews')->get();
        $allPins->map(function($item){
            $tab= explode(' ', strip_tags($item->des), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });
        //dd($allPins);
        //JavaScript::put(compact('allPins'));
        //return view('mobile/map');
        return $allPins;
    }

    public function news()
    {
        $allactus = News::with('PinsActus')->paginate(10);
        //return json_encode(array('data'=>$allactus));
        return view('mobile/news',compact('allactus'));
    }

    public function workeractus()
    {
        $allactus = News::with('PinsActus')->orderBy('datepublie', 'des')->get();
        $allactus->map(function($item){
            $tab= explode(' ', strip_tags($item->des), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });
        return $allactus;
        //return view('mobile/news',compact('allactus'));
    }

    public function search($key)
    {
        //dd($key);
        $results = News::with('PinsActus')->orWhere("title",'LIKE', '%'. $key .'%')->orWhere('des','LIKE', '%'. $key .'%')->get();
        $results->map(function($item){
            $tab= explode(' ', strip_tags($item->des), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });

        return $results;
        //return view('mobile/news',compact('allactus'));
    }

    public function show($id){
        $itemnew = News::find($id);
        //return json_encode(array('data'=>$itemnew));
        return view('mobile/itemnew',compact('itemnew'));
    }

    public function workeractusItem($id)
    {
        $itemnew = News::where('slug',$id)->get();
        //return json_encode(array('data'=>$itemnew));
        return $itemnew;

        /*$itemnew1 = News::where('slug',$id)->first();
        $itemnew = News::where('id',$itemnew1->id)->with('AlbumActus')->get();
        //return json_encode(array('data'=>$itemnew));
        //dd($itemnew);
        return $itemnew;*/
    }

    public function workermedia(){
        $allmedia = Albums::with('albumImages')->with('albumVideo')->get();
        //$allmedia = Albums::get();
        //dd($allmedia);
        return $allmedia;

    }

    public function workermediaItem($id){
        $itemImage = Image::where('albums_id',$id)->get();
        //$nbr = count($itemImage);
        //return json_encode(array('data'=>$itemImage,'nbr'=>$nbr));
        return $itemImage;
    }

    public function workermediaItemvideo($id){
        $itemImage = Video::where('albums_id',$id)->get();
        return $itemImage;
    }

    public function workeractusmediaItem($id)
    {
        $itemnew = News::where('slug',$id)->first();
        $itemmedia = Albums::where('actualite_id',$itemnew->id)->get();
        return $itemmedia;
    }

    public function testmedia(){
        $itemImage = Image::where('id',4)->get();
        return $itemImage;
    }

    public function slideintro(){
        $itemImage = Slide::get();
        return $itemImage;
    }

    public function actuelnew(){
        $itemImage = DB::table('uneactu')->take(1)->orderBy('id', 'desc')->get();
        return $itemImage;
    }

    public function mobilecommune(){
        $allmaquis = Commune::orderBy('commune','asc')->get();
        return $allmaquis;
    }

    public function mobilemaquis($id){
        $allmaquis = Maqui::where('commune_id',$id)->get();
        return $allmaquis;
    }

    /*public function search(){
        return view('mobile/search');
    }*/
}
