<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use ivoirapp\Slide;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $allintros = Slide::all();
        return view('admin/introslide/index',compact('allintros'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        if($request->hasFile('file')){

            try{
                $intro = new Slide();

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'introslide';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $intro->img = $picture;;
                $intro->save();
            }catch (\Exception $e){
                dd($e->getMessage());
            }
            return redirect()->route('slide');

        }else{
            return redirect()->route('slide');
        }

    }

    public function del($id){
        //dd($id);
        $image = Slide::find($id);
        if (!empty($image->img)) {
            $folderName = 'introslide';
            unlink($folderName.'/'.$image->img);
            $image->delete();
            return redirect()->route('slide');
        }
    }
}
