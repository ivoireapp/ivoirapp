<?php

namespace ivoirapp\Http\Controllers;

use Illuminate\Http\Request;
use ivoirapp\Intro;

class IntroslideController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $allintros = '';
        return view('admin/introslide/index',compact('allintros'));
    }

    public function store(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        if($request->hasFile('file')){

            try{
                $intro = new Intro();

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'introslide';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $intro->img = $picture;;
                $intro->save();
            }catch (\Exception $e){
                dd($e->getMessage());
            }
            return redirect()->route('introslide');

        }else{
            return redirect()->route('introslide');
        }

    }

    public function del($id){
        dd($id);
        $image = Intro::find($id);
        if (!empty($image->imgs)) {
            $folderName = 'introslide';
            unlink($folderName.'/'.$image->img);
            $image->delete();
            return redirect()->route('introslide');
        }
    }
}
