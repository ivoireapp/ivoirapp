<?php

namespace ivoirapp;

use Illuminate\Database\Eloquent\Model;

class Maqui extends Model
{
    public function maquisCommune(){
        return $this->belongsTo('ivoirapp\Commune','commune_id');
    }
}
