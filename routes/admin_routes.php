<?php

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('actualites', 'ActualiteController');

Route::resource('news', 'NewController');
Route::post('/newactuel', 'NewController@newactuel')->name('newactuel');

Route::resource('pins', 'PinController');


Route::get('/gallery', 'GallerieController@index')->name('gallery');
Route::post('/gallery', 'GallerieController@store')->name('addgallery');

Route::get('/gallery/{id}/edit', 'GallerieController@edit')->name('editgallery');
Route::post('/gallery/{id}/update', 'GallerieController@update')->name('updategallery');
Route::get('/gallery/{id}/del', 'GallerieController@del')->name('delgallery');

Route::get('/gallery/{slug}', 'GallerieController@show')->name('showgallery');

Route::post('/gallery/addimg', 'GallerieController@storeImg')->name('addimggallery');
Route::post('/gallery/addvideo', 'GallerieController@storeVideo')->name('addvideogallery');

Route::get('/gallery/{id}/delimg', 'GallerieController@delImg')->name('delimggallery');
Route::get('/gallery/{id}/delvideo', 'GallerieController@delVideo')->name('delvideogallery');


Route::get('/slide', 'SlideController@index')->name('slide');
Route::post('/slide', 'SlideController@store')->name('addslide');
Route::get('/slide/{id}/delvideo', 'SlideController@del')->name('delslide');

Route::get('/maquis', 'MaquiController@index')->name('maquis');
Route::post('/maquis', 'MaquiController@store')->name('addmaquis');
Route::get('/maquis/{id}/delmaquis', 'MaquiController@del')->name('delmaquis');

Route::get('/commune', 'CommuneController@index')->name('commune');
Route::post('/commune', 'CommuneController@store')->name('addcommune');
Route::get('/commune/{id}/delcommune', 'CommuneController@del')->name('delcommune');