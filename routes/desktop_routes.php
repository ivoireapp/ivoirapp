<?php

Route::get('map', 'FrontEndController@map');
Route::get('actus', 'FrontEndController@actualites');
Route::get('actu/{slug}', 'FrontEndController@actualite');
Route::get('/', 'FrontEndController@index');