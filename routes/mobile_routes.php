<?php

Route::get('/', 'MobileController@index');
Route::get('map', 'MobileController@map')->name('maps');
Route::get('news', 'MobileController@news')->name('mobileNews');

Route::get('itemnew/{id}', 'MobileController@show')->name('mobileItemNews');
/*Route::get('search', 'MobileController@search')->name('mobileSearch');*/

Route::get('itemnew', 'MobileController@show')->name('mobileItemNews');
Route::get('search', 'MobileController@search')->name('mobileSearch');
/*
Route::get('/', function(){
	return view('index');
});*/
