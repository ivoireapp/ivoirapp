<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Jenssegers\Agent\Agent as Agent;
$agent = new Agent();

if ($agent->isMobile()) {
	require __DIR__.'/mobile_routes.php';
}
else{
	require __DIR__.'/desktop_routes.php';
	require __DIR__.'/admin_routes.php';
}



Route::get('workeractus', 'MobileController@workeractus')->name('workeractus');

Route::get('workermap', 'MobileController@workermap')->name('workermap');

Route::get('search/{key}', 'MobileController@search')->name('search');

Route::get('workeractusItem/{key}', 'MobileController@workeractusItem')->name('workeractusItem');

Route::get('media', 'MobileController@workermedia')->name('workermedia');

Route::get('media/{key}', 'MobileController@workermediaItem')->name('workermediaItem');
Route::get('mediavideo/{key}', 'MobileController@workermediaItemvideo')->name('workermediaItemvideo');

Route::get('workeractusmediaItem/{key}', 'MobileController@workeractusmediaItem')->name('workeractusmediaItem');


Route::get('testmedia', 'MobileController@testmedia')->name('testmedia');

Route::get('slideintro', 'MobileController@slideintro')->name('slideintro');

Route::get('/actuelnew', 'MobileController@actuelnew')->name('actuelnew');

Route::get('mobilecommune', 'MobileController@mobilecommune')->name('mobilecommune');
Route::get('mobilemaquis/{key}', 'MobileController@mobilemaquis')->name('mobilemaquis');