// Map Zooming
var MapZoom = 12;

var map,lastOpenedInfoWindow;
var allMarkers=[];

$(document).ready(function(){
    //alert('de');
    allPinsMobile();
});

function allPinsMobile(){

    mycreatemap();

    var allPins = ivoirapp.allPins;
    console.log(allPins);
    alert('ici');

    allPins.forEach(function(value){
        //console.log(value.bio);
        var lat = value.lat;
        var lon = value.lon;
        var title = value.name;
        var icon= $("#urlwebsite").val();
        //var icon = urltrue+'/'+value.taille_pins.marqueur;

        var LatLng = new google.maps.LatLng(lat,lon);
        /*var date = new Date(value.updated_at),
         yr      = date.getFullYear(),
         month   = date.getMonth(),
         day     = date.getDate(),
         newDate = day+'-'+month+'-'+yr;*/

        var contentString = '<div style="width: 230px;">' +
            '<div class="">' +
            '<div class="d-flex flex-row">' +
            '<div class="sectimg"><img src="piges/'+value.img+'" class="img-lgM rounded" alt="image panneau"></div>' +
            '<div class="ml-3">' +
            '<p class="myp mt-2 text-success font-weight-bold">' + title + '</p>' +
            '<div class="cardMe-text"><p>' + value.minidesc + '</p></div>' +
            '<div class="cardMe-footer"><a href="#" class="btn-map-bg button button-s button-icon regularbold"><i class="fa fa-eye"></i> Voir plus</a></div>' +
            '</div></div></div></div>';
        //var contentString = '<a class="special" href="#"><span><b>' + value.bio + '</b></span></a>';
        allMarkers.push(createMarker(LatLng,icon,title,contentString));
        //createMarker(LatLng,icon,title,contentString);
    });

}

function mycreatemap(){
    map = new google.maps.Map(document.getElementById('idmap'), {
        center: new google.maps.LatLng(5.3607518,-3.9899674),
        //center: new google.maps.LatLng(5.2959048,-3.980355399999999),
        scrollwheel:true,
        zoom: MapZoom,
    });
}

function createMarker(LatLng,icn,title,content){
    /*var infowindow = new google.maps.InfoWindow({
     content: content
     });*/
    var infowindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        icon:icn,
        title: title,
        animation:  google.maps.Animation.DROP
    });

    //markers.push(marker);

    marker.addListener('click', function() {
        closeLastOpenedInfoWindow();
        infowindow.setContent(content);
        infowindow.open(map, marker);
        lastOpenedInfoWindow = infowindow;
    });
}

function closeLastOpenedInfoWindow() {
    if (lastOpenedInfoWindow) {
        lastOpenedInfoWindow.close();
    }
}

/*$('.article-clone .article-back, .close-article').on('click', function(){
 $('.article-clone').removeClass('active-card');
 return false;
 //window.location.href.substr(0, window.location.href.indexOf('#'));
 });*/
