<!DOCTYPE html>
<html lang="fr">
@section('htmlheader')
	@include('mobile.layouts.partials.htmlheader')
@show
<body>
<div id="page-transitions" class="page-build">
    <div class="page-bg gradient-body-1"></div>


@include('mobile.layouts.partials.mainheader')

@yield('main-content')

</div>
@section('scripts')
	@include('mobile.layouts.partials.scripts')
@show
<script>
if ('serviceWorker' in navigator && 'PushManager' in window) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}
</script>
</body>
</html>
