<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>
        @hasSection('htmlheader_title_mobile')@yield('htmlheader_title_mobile') - @endif
        Ivoire Zouglou
    </title>
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/mobile/vendor/styles/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/mobile/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/mobile/vendor/fonts/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front_assets/mobile/vendor/styles/framework.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    @stack('styles')
</head>