<div class="header header-light header-logo-app">
	<a href="{{url('map')}}" class="header-logo"></a>
	<a data-menu="menu-1" data-menu-type="menu-sidebar-left-push" href="#" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
	<a href="#" class="header-icon header-icon-2"><i class="fas fa-search"></i></a>
</div>

<div id="menu-1" class="menu menu-sidebar-left menu-settings">
    <div class="menu-bg gradient-body-1"></div>
    <div class="menu-scroll">

        <div class="menu-header">
            <a class="menu-logo" href="{{url('map')}}"></a>
            <h1>Bienvenu</h1>
            <p>Notre bière, notre style</p>
        </div>
        {{--margin-top de 60px--}}
        {{--<div class="my-mt-60"></div>--}}

        <div class="menu-list icon-background-active">
            {{--<em class="menu-divider top-10">Navigation</em>--}}
            <a class="menu-item {{Route::is('maps') ? 'active-menu' : ''}}" href="{{url('map')}}"><i class="fa fa-map-marker"></i>Carte</a>
            <a class="menu-item {{Route::is('mobileNews') ? 'active-menu' : ''}}" href="{{route('mobileNews')}}"><i class="fa fa-bell"></i>Actualité</a>
            <a class="menu-item" href="#"><i class="fa fa-calendar"></i>Calendrier </a>
            {{--<a class="menu-item" href="interactive.html"><i class="fa gradient-mint-dark fa-hand-point-right"></i>Interactive<span>NEW</span></a>
            <a data-submenu="submenu-1" class="menu-item" href="#"><i class="fa gradient-magenta-light fa-image"></i>Media<span></span></a>
            <div id="submenu-1" class="submenu">
                <a href="galleries.html">Galleries</a>
                <a href="portfolios.html">Portfolios</a>
            </div>
            <a class="menu-item" href="pages.html"><i class="fa gradient-brown-light fa-file"></i>Site Pages</a>
            <a class="menu-item" href="pageapp.html"><i class="fa gradient-blue-light fa-mobile-alt"></i>App Styled</a>
            <a data-submenu="submenu-2" class="menu-item" href="#"><i class="fa gradient-mint-dark fa-shopping-bag"></i>Templates<span></span></a>
            <div id="submenu-2" class="submenu">
                <a href="news.html">News</a>
                <a href="shop.html">Shop</a>
                <a href="blog.html">Blog</a>
            </div>
            <a class="menu-item" href="contact.html"><i class="fa gradient-sky-dark fa-envelope"></i>Contact Us</a>
            <a class="menu-item close-menu" href="#"><i class="fa gradient-red-dark fa-times"></i>Close Menu</a>
            <em class="menu-divider">Get in Touch</em>
            <a class="menu-item" href="tel:+1 234 567 890"><i class="fa gradient-green-light fa-phone"></i>Call Us</a>
            <a class="menu-item" href="sms:+1 234 567 890"><i class="fa gradient-mint-dark fa-comment"></i>SMS Us</a>
            <a class="menu-item" href="mailto:name@domain.com"><i class="fa gradient-sky-dark fa-envelope"></i>Mail Us</a>
            <em class="menu-divider">Let's get Social</em>
            <a class="menu-item" href="https://www.facebook.com/enabled.labs/"><i class="fab facebook-bg fa-facebook-f"></i>Facebook</a>
            <a class="menu-item" href="https://twitter.com/iEnabled"><i class="fab twitter-bg fa-twitter"></i>Twitter</a>
            <a class="menu-item" href="https://plus.google.com/u/1/+EnabledLabs"><i class="fab google-bg fa-google-plus-g"></i>Google+</a>--}}
        </div>
        {{--<em class="menu-copyright">Copyright <span class="copyright-year"></span>, Enabled. All Rights Reserved</em>--}}
    </div>
</div>