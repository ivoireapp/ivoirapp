<script type="text/javascript" 
		src="{{asset('front_assets/mobile/vendor/scripts/jquery.js')}}"></script>
<script type="text/javascript" 
		src="{{asset('front_assets/mobile/vendor/scripts/plugins.js')}}"></script>
<script type="text/javascript" 
		src="{{asset('front_assets/mobile/vendor/scripts/custom.js')}}"></script>
@stack('scripts')