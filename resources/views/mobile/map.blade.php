@extends("mobile.layouts.app")
@section("htmlheader_title_mobile", "Map")

@push('styles')

@endpush

@section("main-content")
    <div class="page-content header-clear-larger">
        <div class="content-box shadow-large bg-white" style="height: 700px;">
            <div id="mymap" class="google-map"></div>
        </div>
        <input type="hidden" name="urlwebsite" id="urlwebsite" value="{{url('front_assets/mobile/images/marker-icon.png')}}">
        {{--<a href="#" class="back-to-top-badge back-to-top-small shadow-large bg-blue-dark"><i class="fa fa-angle-up"></i>Back to Top</a>--}}
    </div>
@endsection

@push('scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
    <script type="text/javascript" src="{{asset('front_assets/mobile/vendor/scripts/myscript.js')}}"></script>

@endpush
