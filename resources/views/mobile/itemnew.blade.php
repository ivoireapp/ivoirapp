@extends("mobile.layouts.app")
@section("htmlheader_title_mobile", "Actualités Detail")

@push('styles')

@endpush

@section("main-content")
    <div class="page-content header-clear-larger">

        <div class="article-card bg-white" style="margin: 0px 15px 0px 15px;border-radius: 0px;">
            <a href="#" class="article-header" style="height:200px;">
                <span class="article-image preload-image shadow-icon-large" data-src-retina="{{url('actualites/'.$itemnew->imgactu)}}" data-src="{{url('actualites/'.$itemnew->imgactu)}}" style="height: 100%;border-radius: 0px;"></span>
            </a>
        </div>

        <div class="content-box bg-white shadow-large reading-time-box reading-words" style="border-radius: 0">
            <div class="top-25 bottom-25">
                <a href="#"><h4 class="bottom-0">{{$itemnew->title}}</h4></a>
                <h4 class="color-blue-dark font-10 bottom-20"><i class="fas fa-clock opacity-50"></i> {{$itemnew->created_at->format('d - m - Y')}}</h4>

                {!! $itemnew->des !!}

                <div class="social-icons top-30 bottom-15">
                    <a href="#" class="icon icon-xs icon-round shadow-icon-large facebook-bg"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="icon icon-xs icon-round shadow-icon-large twitter-bg"><i class="fab fa-twitter"></i></a>
                </div>

                <div class="decoration"></div>
                <div class="clear"></div>

                <a href="{{route('mobileNews')}}" class="back-button button button-s button-full button-round bottom-15 button-blue shadow-icon-large ultrabold uppercase">RETOUR AUX ACTUALITES</a>
            </div>
        </div>

    </div>
@endsection

@push('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
<script type="text/javascript" src="{{asset('front_assets/mobile/vendor/scripts/myscript.js')}}"></script>

@endpush
