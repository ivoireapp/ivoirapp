@extends("mobile.layouts.app")
@section("htmlheader_title_mobile", "Actualités")

@push('styles')

@endpush

@section("main-content")
    <div class="page-content header-clear-larger">
        <div class="content-box content-box-full bg-white shadow-large">
            <div class="search-input-container">
                <div class="search-boxes">
                    <div class="search">
                        <form id="form1" action="#" method="">
                            <i class="fa fa-search"></i>
                            <input onkeypress="pressEnter(event);" class="bg-white no-border" type="text" placeholder="Rechercher...">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-box shadow-large bg-white">
            <div class="top-25 bottom-20">
                <div style="border-bottom: 0.1px solid #e9edf1">
                    <a href="{{route('mobileItemNews')}}"><h4>Ivoire Zouglou dans l'ambiance </h4></a>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <a href="{{route('mobileItemNews')}}" class="bg-highlight" style="margin: -20px 0 10px 0px;">Voir plus <i class="fa fa-angle-right"></i></a>
                    <hr>
                </div>
                <br>

                <div style="border-bottom: 0.1px solid #e9edf1">
                    <a href="{{route('mobileItemNews')}}"><h4>Ivoire Zouglou dans l'ambiance </h4></a>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <a href="{{route('mobileItemNews')}}" class="bg-highlight" style="margin: -20px 0 10px 0px;">Voir plus <i class="fa fa-angle-right"></i></a>
                    <hr>
                </div>
                <br>

                <div style="border-bottom: 0.1px solid #e9edf1">
                    <a href="{{route('mobileItemNews')}}"><h4>Ivoire Zouglou dans l'ambiance </h4></a>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <a href="{{route('mobileItemNews')}}" class="bg-highlight" style="margin: -20px 0 10px 0px;">Voir plus <i class="fa fa-angle-right"></i></a>
                    <hr>
                </div>
                <br>
            </div>
        </div>

        {{--<a href="#" class="back-to-top-badge back-to-top-small shadow-large bg-blue-dark"><i class="fa fa-angle-up"></i>Back to Top</a>--}}
    </div>
@endsection

@push('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
<script type="text/javascript" src="{{asset('front_assets/mobile/vendor/scripts/myscript.js')}}"></script>
<script>
    function pressEnter(event) {
        var code=event.which || event.keyCode; //Selon le navigateur c'est which ou keyCode
        if (code==13) { //le code de la touche Enter
            document.getElementById("form1").submit();
        }
    }
</script>

@endpush
