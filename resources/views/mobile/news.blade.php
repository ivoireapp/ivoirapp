@extends("mobile.layouts.app")
@section("htmlheader_title_mobile", "Actualités")

@push('styles')

@endpush

@section("main-content")
    <div class="page-content header-clear-larger">
        @if(!empty($allactus))
            @foreach($allactus as $actu)
                <div class="content-box content-box-full bg-white shadow-large">
                    <div class="blog-post-home">
                        <a href="{{route('mobileItemNews',$actu->id)}}"><img src="{{url('actualites/'.$actu->imgactu)}}" class="responsive-image">
                        <strong class="font-17">{{$actu->title}}</strong></a>
                        {{--<span>In:<a href="#" class="color-hightlight">Tech, Gadgets, Laptops</a></span>
                        <div class="blog-post-stats">
                            <a href="#"><i class="fa fa-thumbs-up color-blue-light"></i>503</a>
                            <a href="#"><i class="fa fa-eye color-red-light"></i>623</a>
                            <div class="clear"></div>
                        </div>--}}
                        <?php $tab= explode(' ', strip_tags($actu->des), 21);unset($tab[20]); ?>
                        <p>{!! implode(' ', $tab).'...' !!}</p>
                        <a href="#" class="blog-post-home-author1">
                            {{--<img src="{{asset('front_assets/mobile/vendor/images/pictures/isolated/1.jpg')}}" class="responsive-image">
                            <span>By:<u class="color-highlight">Enabled</u></span>--}}
                            <em><i class="fas fa-clock"></i> {{$actu->created_at->format('d - m - Y')}}</em>
                        </a>
                        <a href="{{route('mobileItemNews',$actu->id)}}" class="blog-post-more bg-highlight">Voir plus <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            @endforeach
        @endif

        {{--<div class="content-box content-box-full bg-white shadow-large">
            <div class="blog-post-home">
                <a href="{{route('mobileItemNews')}}"><img src="{{asset('front_assets/mobile/vendor/images/pictures/3.jpg')}}" class="responsive-image">
                    <strong class="font-17">Macbooks are Really Popular</strong></a>
                --}}{{--<span>In:<a href="#" class="color-hightlight">Tech, Gadgets, Laptops</a></span>
                <div class="blog-post-stats">
                    <a href="#"><i class="fa fa-thumbs-up color-blue-light"></i>503</a>
                    <a href="#"><i class="fa fa-eye color-red-light"></i>623</a>
                    <div class="clear"></div>
                </div>--}}{{--
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipi scing elit. Duis quis tempor augue.
                    Phasellus faucibus urna eu tristique feugiat.
                </p>
                <a href="#" class="blog-post-home-author1">
                    --}}{{--<img src="{{asset('front_assets/mobile/vendor/images/pictures/isolated/1.jpg')}}" class="responsive-image">
                    <span>By:<u class="color-highlight">Enabled</u></span>--}}{{--
                    <em><i class="fas fa-clock"></i> 15/10/2020</em>
                </a>
                <a href="{{route('mobileItemNews')}}" class="blog-post-more bg-highlight">Voir plus <i class="fa fa-angle-right"></i></a>
            </div>
        </div>--}}



        {{--<div class="content-box">
            <div class="pagination" style="margin: 5px;">
                <a href="#" class="bg-gray-light"><i class="fa fa-angle-left"></i></a>
                <a href="#">1</a>
                <a href="#" class="bg-blue-dark color-white">2</a>
                <a href="#">3</a>
                <a href="#" class="bg-gray-light"><i class="fa fa-angle-right"></i></a>
            </div>
        </div>--}}
        {{--@if(!empty($allactus))
            <div class="content-box">
                <div class="pagination" style="margin: 5px;">
                    {{ $allactus->links() }}
                </div>
            </div>
        @endif--}}

        {{--<a href="#" class="back-to-top-badge back-to-top-small shadow-large bg-blue-dark"><i class="fa fa-angle-up"></i>Back to Top</a>--}}
    </div>
@endsection

        @push('scripts')
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD27pewB3ValYDzegksy7_VowhtlPKUZJk"></script>
        <script type="text/javascript" src="{{asset('front_assets/mobile/vendor/scripts/myscript.js')}}"></script>

    @endpush
