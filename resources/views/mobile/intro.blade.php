<!DOCTYPE html>
<html>
<head>
	<meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
	<title>Ivoire Zouglou</title>

	<link rel="stylesheet" 
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
			crossorigin="anonymous">

	<script src="https://use.fontawesome.com/c2638299a2.js"></script>

	<link rel="stylesheet" type="text/css" 
				href="{{asset('front_assets/mobile/css/intro.css')}}">
	<link rel="stylesheet" type="text/css" 
				href="{{asset('front_assets/mobile/css/animate.css')}}">
</head>
<body>
<div id="main-intro">
	<div class="bg">
		<img src="{{asset('front_assets/mobile/images/logo-intro.png')}}">
		<a href="{{url('map')}}">
			<i class="fa fa-angle-right fa-2x" id="talkbubble"></i>
		</a>
	</div>
</div>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{asset('front_assets/mobile/js/intro.js')}}"></script>
<script>
if ('serviceWorker' in navigator) {
	console.log('service-worker')
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('service-worker.js').then(function(registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}
</script>
</body>
</html>