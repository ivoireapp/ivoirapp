@extends("desktop.layouts.app")
@section("htmlheader_title", "Actualités")

@push('styles')
<style type="text/css">
	.pagination{
		margin-top: 35px;
	    text-align: center;
	    display: -webkit-inline-box;
	}
	.pagination-flat-primary .page-item .page-link{
		color: #1E7DC3;
	}
	.pagination-flat-primary .page-item .page-link:hover,
	.pagination-flat-primary .page-item.active .page-link{
		background-color: #1E7DC3;
		color: #fff;
	}
	.actu-single .time {
	    display: block;
	    margin-left: 5px;
	    margin-top: 15px;
	    font-size: 14px;
	    color: #b6b4b4;
	}
</style>
@endpush

@section("main-content")	
<!-- Breadcrumb-->
<div class="row pt-2 pb-2">
	<div class="col-sm-9">
	    <h4 class="page-title">{{$actu[0]['title']}}</h4>
	</div>
</div>
<!-- End Breadcrumb-->

<div class="row actu-single" style="margin: 0">
	<div class="col-lg-12" style="padding: 0">
	  <div class="card">
	      <div class="card-body">
	      		<div class="row">
	      			@foreach($actu as $data)
		      		<div class="col-lg-12">
		      			<img src="{{ asset('actualites/'.$data->imgactu)}}" />
		      			<span class="time">
		      				<?php 
		      					$exp = explode(' ', $data->created_at);
		      					$date = explode('-', $exp[0]);
		      				?>
	      					<strong>
	      						Publié le: <?php echo $date[2].'.'.$date[1].'.'.$date[0]; ?>
      						</strong>
	      				</span>
		      		</div>
		      		<div class="col-lg-12">
		      			<div class="row">
		      				<p>
		      					{!!$data->des!!}
		      				</p>
		      			</div>
		      			<div class="row">
		      				<ul class="social" 
		      						style="padding-left: 20px;
		      								margin-top: 10px;
	    									margin-bottom: 0;">
	    									 
		      					<li style="width: auto;
										    font-size: 16px;
										    font-weight: 600;
										    margin-right: 15px;">Partager</li>
								<li class="footer-icon-facebook">
									<a target="_blank" href="#">
										<i class="fa fa-facebook-f" aria-hidden="true"></i>
									</a>
								</li> 
								<li class="footer-icon-googleplus">
									<a target="_blank" href="#">
										<i class="fa fa-google-plus" aria-hidden="true"></i>		
									</a>
								</li> 
								<li class="footer-icon-twitter">
									<a target="_blank" href="#">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li> 
							</ul> 
		      			</div>
		      		</div>
		      		@endforeach
	      		</div>
	      </div>
	  </div>
	</div>
</div>
@endsection

@push('scripts')

@endpush