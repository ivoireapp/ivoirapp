<!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
      <div class="brand-logo">
        <a href="{{url('/')}}">
          <img src="{{ asset('front_assets/desktop/assets/images/logo-ivoire.png')}}" class="logo-icon" alt="logo icon">
        </a>
	   </div>

	 <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">MENU</li>
      <li class="{{Request::is('/') ? 'active' : ''}}">
        <a href="{{url('/')}}" class="waves-effect">
          <i class="icon-home"></i><span>Accueil</span>
        </a>
      </li>

      <li class="{{Request::is('map') ? 'active' : ''}}">
        <a href="{{url('map')}}" class="waves-effect">
          <i class="icon-map"></i><span>Carte</span>
        </a>
      </li>

      <li class="{{Request::is('actus') ? 'active' : ''}}">
        <a href="{{url('actus')}}" class="waves-effect">
          <i class="icon-calendar"></i><span>Actualités</span>
        </a>
      </li>
    </ul>
	 
   </div>
   <!--End sidebar-wrapper-->