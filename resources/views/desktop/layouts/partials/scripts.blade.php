
</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->

<!--Start footer-->
	<!-- <footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2018 Ivoire Zouglou
        </div>
      </div>
    </footer> -->
	<!--End footer-->
   
  </div><!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('front_assets/desktop/assets/js/jquery.min.js')}}"></script>
  <script src="{{ asset('front_assets/desktop/assets/js/bootstrap.min.js')}}"></script>
	
  <!-- simplebar js -->
  <script src="{{ asset('front_assets/desktop/assets/plugins/simplebar/js/simplebar.js')}}"></script>
  <!-- waves effect js -->
  <script src="{{ asset('front_assets/desktop/assets/js/waves.js')}}"></script>
  <!-- sidebar-menu js -->
  <script src="{{ asset('front_assets/desktop/assets/js/sidebar-menu.js')}}"></script>
  <!-- Custom scripts -->
  <script src="{{ asset('front_assets/desktop/assets/js/app-script.js')}}"></script>
  
@stack('scripts')