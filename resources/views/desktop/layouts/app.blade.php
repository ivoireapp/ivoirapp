<!DOCTYPE html>
<html lang="fr">
@section('htmlheader')
	@include('desktop.layouts.partials.htmlheader')
@show
<body>
	<div id="wrapper">
		@include('desktop.layouts.partials.sidebar')
		@include('desktop.layouts.partials.mainheader')

		@yield('main-content')

		@section('scripts')
			@include('desktop.layouts.partials.scripts')
		@show
	</div>
</body>
</html>
