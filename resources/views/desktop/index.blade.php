@extends("desktop.layouts.app")
@section("htmlheader_title", "Accueil")

@push('styles')
<link rel="stylesheet" type="text/css" 
		href="{{ asset('front_assets/desktop/assets/flexslider/flexslider.css')}}">
<style type="text/css">
.btn-gradient-dusk{
    background: #1574BE !important;
    background: -webkit-linear-gradient(45deg, #1574BE, #f1582e)!important;
    background: linear-gradient(45deg, #1574BE, #f1582e)!important;
    color: #fff !important;
    border: 0;
    position: relative;
    top: 85px;
    padding: 15px 30px;
    font-size: 13px;
}
.actu-home{
	height: 213px;
	background-size: cover;
	background-position: center center;
}
.actu-home a{
	display: block;
	width: 100%;
    height: 100%;
}

.cardMe {
	font-size: 1em;
	overflow: hidden;
	padding: 0;
	border: none;
	border-radius: .28571429rem;
	box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
	width: 285px;
}

.cardMe-footer .float-right {
	float: right!important;
}
.img-lgM {
	width: 100%;
	height: 100%;
}
.sectimg {
	width: auto;
	height: 110px;
}

.cardMe-block {
	font-size: 1em;
	position: relative;
	margin: 0;
	padding: 1em;
	border: none;
	/*border-top: 1px solid rgba(34, 36, 38, .1);*/
	box-shadow: none;
}

.cardMe-img-top {
	display: block;
	width: 100%;
	height: auto;
}

.cardMe-title {
	font-size: 1.28571429em;
	font-weight: 700;
	line-height: 1.2857em;
}

.cardMe-text {
	clear: both;
	margin-top: .5em;
	margin-bottom: 1.1em!important;
	color: rgba(0, 0, 0, .68);
	text-align: justify;
}

.cardMe-inverse .btn {
	border: 1px solid rgba(0, 0, 0, .05);
}
.text-success {
	color: #f1582e !important;
}
.font-weight-bold {
	font-weight: 700 !important;
}
.mt-2{
	margin-top: 0.5rem !important;
}

.d-flex.flex-row {
	flex-direction: row !important;
	display: grid!important;
}
.myp{
	margin-bottom: 0px!important;
}

.cardMe-text p{
	margin-bottom: 0px!important;
}

</style>
@endpush

@section("main-content")	
<!-- Breadcrumb-->
<div class="row pt-2 pb-2">
	<div class="col-sm-9">
	    <h4 class="page-title">Accueil</h4>
	</div>
</div>
<!-- End Breadcrumb-->
<div class="row">
	<div class="col-lg-12">
	  <div class="card">
	     <div class="card-header text-uppercase">Dernières actualités</div>
	      <div class="card-body">
	        	<!-- Place somewhere in the <body> of your page -->
				<div class="flexslider carousel">
				  <ul class="slides">
				  	
				  	@foreach($actus as $actu)
				    <li style="background-image: url('actualites/{{$actu->imgactu}}')" class="actu-home" 
				    		title="{{$actu->slug}}">
				    	<a href="{{url('actu/'.$actu->slug)}}">
					      	<h4>
					      		{{$actu->title}}
				      		</h4>
				      	</a>
				    </li>
				    @endforeach

				    <li class="all-rooms-home">
				    	<a hre="{{url('actualites')}}" class="btn btn-gradient-dusk waves-effect waves-light m-1">
				    		Toutes les actualités
				    	</a>
				    </li>
				    
				  </ul>
				</div>
	      </div>
	  </div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
	  <div class="card">
	     <div class="card-header text-uppercase">Map</div>
	      <div class="card-body">
			  <div id="idmap" class="gmaps"></div>
			  <input type="hidden" name="urlwebsite" id="urlwebsite" value="{{url('front_assets/mobile/images/marker-icon.png')}}">
		  </div>
	  </div>
	</div>
</div>
<!--End Row-->
@endsection

@push('scripts')
<!-- google maps api -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBimFeo5322NEl7d8SPjHBEtJ9pFAa60VY&callback=allPinsMobile"></script>
<script src="{{ asset('front_assets/desktop/assets/flexslider/jquery.flexslider.js')}}"></script>
<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 320,
    itemMargin: 5,
    slideshow: true
  });
});

var MapZoom = 12;

var map,lastOpenedInfoWindow;
var allMarkers=[];


function allPinsMobile(){

    mycreatemap();

    var allPins = ivoirapp.allPins;
    console.log(allPins);


    allPins.forEach(function(value){
        //console.log(value.bio);
        var lat = value.lat;
        var lon = value.lon;
        var title = value.name;
        var icon= $("#urlwebsite").val();
        //var icon = urltrue+'/'+value.taille_pins.marqueur;

        var LatLng = new google.maps.LatLng(lat,lon);
		/*var date = new Date(value.updated_at),
		 yr      = date.getFullYear(),
		 month   = date.getMonth(),
		 day     = date.getDate(),
		 newDate = day+'-'+month+'-'+yr;*/

        var contentString = '<div style="width: 230px;">' +
            '<div class="">' +
            '<div class="d-flex flex-row">' +
            '<div class="sectimg"><img src="piges/'+value.img+'" class="img-lgM rounded" alt="image panneau"></div>' +
            '<div class="ml-3">' +
            '<p class="myp mt-2 text-success font-weight-bold">' + title + '</p>' +
            '<div class="cardMe-text"><p>' + value.minidesc + '</p></div>' +
            //'<div class="cardMe-footer"><a href="#" class="btn-map-bg button button-s button-icon regularbold"><i class="fa fa-eye"></i> Voir plus</a></div>' +
            '</div></div></div></div>';
        //var contentString = '<a class="special" href="#"><span><b>' + value.bio + '</b></span></a>';
        allMarkers.push(createMarker(LatLng,icon,title,contentString));
        //createMarker(LatLng,icon,title,contentString);
    });

}

function mycreatemap(){
    console.log('mycreatemap');
    map = new google.maps.Map(document.getElementById('idmap'), {
        center: new google.maps.LatLng(5.3607518,-3.9899674),
        scrollwheel:true,
        zoom: MapZoom,
    });
}

function createMarker(LatLng,icn,title,content){
	/*var infowindow = new google.maps.InfoWindow({
	 content: content
	 });*/
    var infowindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        icon:icn,
        title: title,
        animation:  google.maps.Animation.DROP
    });

    //markers.push(marker);

    marker.addListener('click', function() {
        closeLastOpenedInfoWindow();
        infowindow.setContent(content);
        infowindow.open(map, marker);
        lastOpenedInfoWindow = infowindow;
    });
}

function closeLastOpenedInfoWindow() {
    if (lastOpenedInfoWindow) {
        lastOpenedInfoWindow.close();
    }
}

</script>
@endpush