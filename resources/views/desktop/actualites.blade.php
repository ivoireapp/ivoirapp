@extends("desktop.layouts.app")
@section("htmlheader_title", "Actualités")

@push('styles')
<style type="text/css">
	.pagination{
		margin-top: 35px;
	    text-align: center;
	    display: -webkit-inline-box;
	}
	.pagination-flat-primary .page-item .page-link{
		color: #1E7DC3;
	}
	.pagination-flat-primary .page-item .page-link:hover,
	.pagination-flat-primary .page-item.active .page-link{
		background-color: #1E7DC3;
		color: #fff;
	}
	.block-cover{
		height: 242px; 
	}
	.cover{
	    background-image: url(http://localhost/ivoirapp/public/actualites/X2JURWvx.jpg);
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    top: 0;
	    background-position: center center;
	    background-size: cover;
	}
</style>
@endpush

@section("main-content")	
<!-- Breadcrumb-->
<div class="row pt-2 pb-2">
	<div class="col-sm-9">
	    <h4 class="page-title">Actualités</h4>
	</div>
</div>
<!-- End Breadcrumb-->

<div class="row" style="margin: 0">
    <div class="lightboxgallery-gallery clearfix bg-white shadow">
    	@foreach($actus as $actu)
        <a class="lightboxgallery-gallery-item" href="{{url('actu/'.$actu->slug)}}">
          <div class="block-cover">
            <img src="{{ asset('actualites/'.$actu->imgactu) }}" 
            	 style="visibility: hidden;">
        	<div class="cover" 
        		style="background-image: url({{ asset('actualites/'.$actu->imgactu) }});">
    		</div>
            <div class="lightboxgallery-gallery-item-content">
              <span class="lightboxgallery-gallery-item-title">
              	{{$actu->title}}
			  </span>
            </div>
          </div>
        </a>
        @endforeach
    </div>
</div>
<div class="row">
	<div class="col-12" style="text-align: center;">
		{{ $actus->links() }}
	</div>
</div>
@endsection

@push('scripts')

@endpush