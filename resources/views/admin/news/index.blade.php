@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')

            {{--ADD MODAL--}}
            <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ajouter une actualité
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>

                        <form action="{{route('news.store')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="input-group mb-3 col-md-12">
                                    <input type="text" name="title" class="form-control" required="" placeholder="Titre">
                                </div>

                                <div class="input-group mb-3 col-md-12">
                                    {{--<input type="text" name="lon" class="form-control" required="" placeholder="Description">--}}
                                    <label for="ckeditor_full"><strong>Description</strong> </label>
                                    <textarea id="ckeditor_full" name="contenu" class="form-control" rows="3" style="width: 100%"></textarea>
                                </div>

                                <div class="input-group mb-3 col-md-12" style="padding-left: 0px">
                                    <label class="col-md-12" for="idpins" style="padding-left: 0px"><strong>Choissisez le pin</strong> </label>
                                    <select name="idpins" id="idpins" class="form-control">
                                        @foreach($allpins as $pin)
                                            <option value="{{$pin->id}}">{{$pin->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="file"><strong>Photo de l'actualité</strong></label>
                                    <input type="file" id="file" name="file" class="dropify" required/>
                                </div>

                                <div class="input-group mb-3 col-md-12" style="padding-left: 0px">
                                    <label class="col-md-12" for="datepublie" style="padding-left: 0px"><strong>Date de publication</strong></label>
                                    <input type="date" name="datepublie" class="form-control" required="">
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="adduser" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Choisir l'actualité à la une
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>

                        <form action="{{route('newactuel')}}" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="input-group mb-3 col-md-12" style="padding-left: 0px">
                                    <label class="col-md-12" for="idpins" style="padding-left: 0px"><strong>Choissisez l'actualité</strong> </label>
                                    <select name="idactunew" id="idactunew" class="form-control">
                                        @foreach($allactus as $actu)
                                            <option value="{{$actu->slug}}">{{$actu->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="addr" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>ACTUALITES</h1>
                                    <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#modal2" style="margin-bottom: 2px;margin-left: 10px">
                                        <i class="fa fa-plus"></i> &nbsp; CHOISIR L'ACTUALITE A LA UNE
                                    </button>

                                    <button type="button" class="btn btn-outline-default float-right" data-toggle="modal" data-target="#modal1" style="margin-bottom: 2px">
                                        <i class="fa fa-plus"></i> &nbsp; AJOUTER
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ref.No.</th>
                                        <th style="width: 400px">Titre</th>
                                        <th>Pin</th>
                                        <th>Vue</th>
                                        <th>Like</th>
                                        <th>Dislike</th>
                                        <th>Date publication</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @if(!empty($allactus))
                                        <tbody>
                                            @foreach($allactus as  $k =>$actu)
                                                <tr>
                                                    <td>#{{$k+1}}</td>
                                                    <td>{{$actu->title}}</td>
                                                    <td>{{$actu->PinsActus->name}}</td>
                                                    <td>{{$actu->view ? $actu->view : '0'}}</td>
                                                    <td>{{$actu->likes ? $actu->likes : '0'}}</td>
                                                    <td>{{$actu->dislikes ? $actu->dislikes : '0'}}</td>
                                                    <td>{{ date("d/m/Y",strtotime($actu->datepublie))}}</td>
                                                    <td>{!!  $actu->status=='en attente' ? '<span class="badge badge-danger">En attente</span>' : '<span class="badge badge-success">Publie</span>' !!}</td>
                                                    <td>
                                                        <a href="" {{--{{route('news.show',$actu->id)}}--}} class="btn btn-outline-success"><i class="fa fa-eye text-success"></i> </a>
                                                        <a href="{{route('news.edit',$actu->id)}}" class="btn btn-outline-primary"><i class="fa fa-edit text-primary"></i> </a>
                                                        <button class="btn btn-outline-danger" data-toggle="modal" data-target="#modal-{{$k+1}}"><i class="fa fa-trash text-danger"></i></button>

                                                        <div class="modal fade" id="modal-{{$k+1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header bg-danger border-0">
                                                                        <h5 class="modal-title text-white">Confirmation</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>

                                                                    <div class="modal-body p-5">
                                                                        Voulez-vous réellement supprimer cette actualité.
                                                                    </div>

                                                                    <div class="modal-footer border-0">
                                                                        <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                                                                        <form action="{{route('news.destroy',$actu->id)}}" method="POST">
                                                                            @csrf
                                                                            <input name="_method" type="hidden" value="DELETE">
                                                                            <button type="submit" class="btn btn-danger">Oui</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ URL::asset('vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
        var editor_config = {
            height: 100,
            path_absolute : "/",
            selector: "#ckeditor_full",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "formatselect | insertfile undo redo | styleselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);

    </script>
@endsection
