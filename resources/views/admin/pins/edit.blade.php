@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')

            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>MODIFIER UN PINS</h1>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <form action="{{route('pins.update',$itempin->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">
                                <div class="modal-body">
                                    <div class="input-group mb-3 col-md-12">
                                        <label class="col-md-12" for="name"><strong>Nom de l'espace</strong> </label>
                                        <input value="{{$itempin->name}}" type="text" name="name" class="form-control" required="" placeholder="Nom de l'espace">
                                    </div>

                                    <div class="input-group mb-3 col-md-12">
                                        <label class="col-md-12" for="latitude"><strong>Latitude</strong> </label>
                                        <input value="{{$itempin->lat}}" type="text" name="lat" class="form-control" required="" placeholder="Latitude">
                                    </div>

                                    <div class="input-group mb-3 col-md-12">
                                        <label class="col-md-12" for="Longitude"><strong>Longitude</strong> </label>
                                        <input value="{{$itempin->lon}}" type="text" name="lon" class="form-control" required="" placeholder="Longitude">
                                    </div>

                                    <div class="input-group mb-3 col-md-12">
                                        {{--<input type="text" name="lon" class="form-control" required="" placeholder="Description">--}}
                                        <label for="ckeditor_full"><strong>Description</strong> </label>
                                        <textarea id="ckeditor_full" name="contenu" class="form-control" rows="3" style="width: 100%">{{$itempin->des}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="file"><strong>Photo du panneau</strong></label>
                                        <input type="file" id="file" name="file" class="dropify" data-default-file="{{url('piges/'.$itempin->img)}}"/>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="submit" name="adduser" class="btn btn-success"><i class="fa fa-save"></i> MISE A JOUR</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer')
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ URL::asset('vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
        var editor_config = {
            height: 100,
            path_absolute : "/",
            selector: "#ckeditor_full",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "formatselect | insertfile undo redo | styleselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);

    </script>
@endsection