@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')

            {{--ADD MODAL--}}
            <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ajouter une gallery
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="{{route('addgallery')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="input-group mb-3 col-md-12">
                                    <input type="text" name="title" class="form-control" required="" placeholder="Titre">
                                </div>

                                <div class="input-group mb-3 col-md-12" style="padding-left: 0px">
                                    <label class="col-md-12" for="idpins" style="padding-left: 0px"><strong>Choissisez l'actualités</strong> </label>
                                    <select name="idactu" id="idactu" class="form-control">
                                        @foreach($allactu as $actu)
                                            <option value="{{$actu->id}}">{{$actu->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="adduser" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>GALLERY</h1>
                                    <button type="button" class="btn btn-outline-default float-right" data-toggle="modal" data-target="#modal1" style="margin-bottom: 2px">
                                        <i class="fa fa-plus"></i> &nbsp; AJOUTER
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ref.No.</th>
                                        <th>Titre</th>
                                        <th>slug</th>
                                        <th>Actualites</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @if(!empty($allactus))
                                        <tbody>
                                        @foreach($allactus as  $k =>$actu)
                                            <tr>
                                                <td>#{{$k+1}}</td>
                                                <td>{{$actu->title}}</td>
                                                <td>{{$actu->slug}}</td>
                                                <td>{{$actu->albumActus->title}}</td>
                                                <td>
                                                    <a href="{{route('showgallery',$actu->slug)}}" class="btn btn-outline-primary"><i class="fa fa-eye text-primary"></i> </a>
                                                    <a href="{{route('editgallery',$actu->slug)}}" class="btn btn-outline-success"><i class="fa fa-edit text-success"></i> </a>
                                                    <a href="{{route('delgallery',$actu->slug)}}" class="btn btn-outline-danger"><i class="fa fa-trash text-danger"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection
