@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')

            {{--ADD MODAL--}}
            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ajouter une image
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>

                        <form action="{{route('addimggallery')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="file"><strong>Photo</strong></label>
                                    <input type="file" id="file" name="file" class="dropify" required/>
                                </div>

                                <input type="hidden" name="idalbum" value="{{$albums->id}}">

                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="adduser" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ajouter une video
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>

                        <form action="{{route('addvideogallery')}}" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="input-group mb-3 col-md-12">
                                    <input type="url" name="urlyoutube" class="form-control" required="" placeholder="Lien youtube">
                                </div>
                                <input type="hidden" name="idalbum" value="{{$albums->id}}">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="adduser" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>ALBUMS {{$albums->title}}</h1>
                                    <button type="button" class="btn btn-outline-default float-right" data-toggle="modal" data-target="#modal" style="margin-bottom: 2px;margin-left: 10px">
                                        <i class="fa fa-plus"></i> &nbsp; AJOUTER IMAGE
                                    </button>
                                    <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#modal1" style="margin-bottom: 2px">
                                        <i class="fa fa-plus"></i> &nbsp; AJOUTER VIDEO
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <h1>Liste des images</h1>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ref.No.</th>
                                        <th>Album</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @if(!empty($images))
                                        <tbody>
                                        @foreach($images as  $k =>$img)
                                            <tr>
                                                <td>#{{$k+1}}</td>
                                                <td>{{$img->ImageAlbums->title}}</td>
                                                <td><img src="{{url('galleries/'.$img->imgs) }}" alt="photo" width="100px" height="70px"></td>
                                                <td>
                                                    <a href="{{route('delimggallery',$img->id)}}" class="btn btn-outline-danger"><i class="fa fa-trash text-danger"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <h1>Liste des videos</h1>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ref.No.</th>
                                        <th>Album</th>
                                        <th>lien</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @if(!empty($videos))
                                        <tbody>
                                        @foreach($videos as  $k =>$video)
                                            <tr>
                                                <td>#{{$k+1}}</td>
                                                <td>{{$video->videoAlbums->title}}</td>
                                                <td><a href="https://www.youtube.com/watch?v={{$video->url }}" target="_blank" class="btn btn-primary">Voir</a></td>
                                                <td>
                                                    <a href="{{route('delvideogallery',$video->id)}}" class="btn btn-outline-danger"><i class="fa fa-trash text-danger"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
