@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>EDITE UN ALBUM</h1>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                        <form action="{{route('updategallery',$itemAlbums->id)}}" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="input-group mb-3 col-md-12">
                                    <label class="col-md-12" for="idpins" style="padding-left: 0px"><strong>Titre</strong> </label>
                                    <input type="text" value="{{$itemAlbums->title}}" name="title" class="form-control" required="" placeholder="Titre">
                                </div>

                                <div class="input-group mb-3 col-md-12" style="padding-left: 0px">
                                    <label class="col-md-12" for="idpins" style="padding-left: 0px"><strong>Choissisez l'actualités</strong> </label>
                                    <select name="idactu" id="idactu" class="form-control">
                                        @foreach($allactus as $actu)
                                            <option value="{{$actu->id}}" {{$actu->id==$itemAlbums->actualite_id ? 'selected' : '' }}>{{$actu->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="adduser" class="btn btn-primary"><i class="fa fa-save"></i> UPDATE</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection
