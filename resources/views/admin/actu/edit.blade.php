@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>MODIFIER UNE ACTUALITE</h1>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <form action="{{route('actualites.update',$itemactu->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">
                                <div class="modal-body">
                                    <div class="input-group mb-3 col-md-12">
                                        <label class="col-md-12" for="title" style="padding-left: 0px"><strong>Titre</strong> </label>
                                        <input type="text" name="title" class="form-control" value="{{$itemactu->title}}" required="" placeholder="Titre">
                                    </div>

                                    <div class="input-group mb-3 col-md-12">
                                        {{--<input type="text" name="lon" class="form-control" required="" placeholder="Description">--}}
                                        <label for="ckeditor_full"><strong>Description</strong> </label>
                                        <textarea id="ckeditor_full" name="contenu" class="form-control" rows="3" style="width: 100%">{{$itemactu->des}}</textarea>
                                    </div>

                                    <div class="input-group mb-3 col-md-12" style="padding-left: 0px">
                                        <label class="col-md-12" for="idpins" style="padding-left: 0px"><strong>Choissisez le pin</strong> </label>
                                        <select name="idpins" id="idpins" class="form-control">
                                            @foreach($allpins as $pin)
                                                <option value="{{$pin->id}}" {{$itemactu->pin_id==$pin->id ? 'selected' : '' }}>{{$pin->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="file"><strong>Photo de l'actualité</strong></label>
                                        <input type="file" id="file" name="file" class="dropify" data-default-file="{{url('actualites/'.$itemactu->imgactu)}}"/>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="submit" name="adduser" class="btn btn-success"><i class="fa fa-save"></i> MISE A JOUR</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script src="{{ URL::asset('vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
        var editor_config = {
            height: 100,
            path_absolute : "/",
            selector: "#ckeditor_full",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "formatselect | insertfile undo redo | styleselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);

    </script>
@endsection
