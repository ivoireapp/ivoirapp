@extends('layouts.admin')

@section('header')
    <link rel="stylesheet" href="{{ asset('css/dropify.css') }}">
@endsection

@section('contentadmin')
    <div class="page-wrapper">
        @include('includes/admin/nav')

        <div class="main-container">
            @include('includes/admin/sidebar')

            {{--ADD MODAL--}}
            <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Ajouter une commune</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="{{route('addcommune')}}" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="file"><strong>Commune</strong></label>
                                    <input type="text" id="commune" name="commune" class="form-control" required/>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="adduser" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-0" style="padding: 1rem!important">
                                    <h1>COMMUNES</h1>
                                    <button type="button" class="btn btn-outline-default float-right" data-toggle="modal" data-target="#modal1" style="margin-bottom: 2px">
                                        <i class="fa fa-plus"></i> &nbsp; AJOUTER
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card-header bg-light row" id="resultat">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ref.No.</th>
                                        <th>Commune</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @if(!empty($allcommunes))
                                        <tbody>
                                        @foreach($allcommunes as  $k =>$actu)
                                            <tr>
                                                <td>#{{$k+1}}</td>
                                                <td>{{$actu->commune}}</td>
                                                <td>
                                                    <a href="{{route('delcommune',$actu->id)}}" class="btn btn-outline-danger"><i class="fa fa-trash text-danger"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/dropify.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
