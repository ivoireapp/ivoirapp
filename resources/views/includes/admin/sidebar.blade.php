<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">Navigation</li>

            <li class="nav-item">
                <a href="{{url('/home')}}" class="nav-link {{Route::is('home') ? 'active' : ''}}">
                    <i class="icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item">
                <a href="{{route('pins.index')}}" class="nav-link {{Route::is('pins.index') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Pins
                </a>
            </li>


            {{--<li class="nav-item">
                <a href="{{route('actualites.index')}}" class="nav-link {{Route::is('actualites.index') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Actualités
                </a>
            </li>--}}

            <li class="nav-item">
                <a href="{{route('news.index')}}" class="nav-link {{Route::is('news.index') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Actualités
                </a>
            </li>

            <li class="nav-item">
                <a href="{{route('gallery')}}" class="nav-link {{Route::is('gallery') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Gallery
                </a>
            </li>

            <li class="nav-item">
                <a href="{{route('slide')}}" class="nav-link {{Route::is('slide') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Intro Slide
                </a>
            </li>

            <li class="nav-item">
                <a href="{{route('commune')}}" class="nav-link {{Route::is('commune') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Communes
                </a>
            </li>

            <li class="nav-item">
                <a href="{{route('maquis')}}" class="nav-link {{Route::is('maquis') ? 'active' : ''}}">
                    <i class="icon icon-puzzle"></i> Maquis
                </a>
            </li>

           {{-- <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-target"></i> Layouts <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="layouts-normal.html" class="nav-link">
                            <i class="icon icon-target"></i> Normal
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="layouts-fixed-sidebar.html" class="nav-link">
                            <i class="icon icon-target"></i> Fixed Sidebar
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="layouts-fixed-header.html" class="nav-link">
                            <i class="icon icon-target"></i> Fixed Header
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="layouts-hidden-sidebar.html" class="nav-link">
                            <i class="icon icon-target"></i> Hidden Sidebar
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-energy"></i> UI Kits <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="alerts.html" class="nav-link">
                            <i class="icon icon-energy"></i> Alerts
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="buttons.html" class="nav-link">
                            <i class="icon icon-energy"></i> Buttons
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="cards.html" class="nav-link">
                            <i class="icon icon-energy"></i> Cards
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="modals.html" class="nav-link">
                            <i class="icon icon-energy"></i> Modals
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="tabs.html" class="nav-link">
                            <i class="icon icon-energy"></i> Tabs
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="progress-bars.html" class="nav-link">
                            <i class="icon icon-energy"></i> Progress Bars
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="widgets.html" class="nav-link">
                            <i class="icon icon-energy"></i> Widgets
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-graph"></i> Charts <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="chartjs.html" class="nav-link">
                            <i class="icon icon-graph"></i> Chart.js
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="forms.html" class="nav-link">
                    <i class="icon icon-puzzle"></i> Forms
                </a>
            </li>

            <li class="nav-item">
                <a href="tables.html" class="nav-link">
                    <i class="icon icon-grid"></i> Tables
                </a>
            </li>

            <li class="nav-title">More</li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-umbrella"></i> Pages <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="blank.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Blank Page
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="login.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Login
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="register.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Register
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="invoice.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Invoice
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="404.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> 404
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="500.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> 500
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="settings.html" class="nav-link">
                            <i class="icon icon-umbrella"></i> Settings
                        </a>
                    </li>
                </ul>
            </li>--}}
        </ul>
    </nav>
</div>